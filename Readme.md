NLP of university syllabi
=========================

## References

* "Machine Learning-based Syllabus Classification toward Automatic Organization of Issue-oriented Interdisciplinary Curricula" ([doi:10.1016/j.sbspro.2011.10.604](https://www.sciencedirect.com/science/article/pii/S1877042811024311))
* "Understanding knowledge areas in curriculum through text mining from course materials" ([doi:10.1109/TALE.2016.7851788](https://ieeexplore.ieee.org/abstract/document/7851788))
* "Curriculum Analysis of Computer Science Departments by Simplified, Supervised LDA" ([doi:10.2197/ipsjjip.26.497](https://www.jstage.jst.go.jp/article/ipsjjip/26/0/26_497/_article))

## To do

* experiment more with input models and similarity metrics
* use higher-order structures&mdash;such as `spacy` noun chunks&mdash;as bases for the vector space
* ~~migrate the extracted texts to a database or a pickle disk file~~ (The new [`ind.py`](ind.py) expects a pickle file to already have been created by [`generate_corpus.py`](generate_corpus.py).)
* migrate the spacy-processed text docs (but not the `bases` function) to a pickle file

## Introduction

In order to search for "interdisciplinarity" in course offerings, publicly-available syllabi have been collected from a variety of (mostly large, Canadian) universities.
The collected syllabi have been used to define academic disciplines in the following manner: any course offered by a Sociology department is assumed to be representative of a sociology course, any course offered by a Physics department is assumed to be representative of a physics course, etc.
The syllabi are parsed for their useful words, then vectors and models are built.
At time of writing,

```
 The dictionary has 2028 words from 1616 syllabi.
```

The code looks to the directory `web_syllabi` for training material.
This directory should be populated with subdirectories that are the disciplinary bins, such as `ARTS` and `ENGL`.
I have not included my contents of `web_syllabi` in this repo; although, here are my bins (to date):

```bash
$ ls web_syllabi
ARTS/  CHEM/  EDUC/  GEOL/  MATH/  PHYS/  PSYC/
BIOL/  ECON/  ENGL/  HIST/  PHIL/  POLI/  SOAN/
```

In the graphics below, the number in parentheses for each discipline is the number of syllabi in that subdirectory.

## Methodology

The model is used to compare an input/target syllabus with those in the training set.
Any syllabus in the training set that turns up as "similar" to the target syllabus (as per chosen metrics and thresholds) has its similarity score incorporated into a tally that is tied to the academic discipline from which it came (Sociology or Physics or whatever).
The target syllabus is then identified by the tallies of similarities from the various academic disciplines.
For example, my [introductory](https://gitlab.com/imhoffman/fa19b2-ind3156/wikis/home) and [intermediate](https://gitlab.com/imhoffman/fa19b4-mat3006/wikis/home) computer programming offerings are tallied as shown here:

![images/Fa19B2-Programming-Syllabus-spider-score.png](images/Fa19B2-Programming-Syllabus-spider-score.png)
![images/Fa19B4-Advent-Syllabus-spider-score.png](images/Fa19B4-Advent-Syllabus-spider-score.png)

Note that the radial/vertical scales are different&mdash;many matches make for a large score.
The scores are *not* normalized because absolute score is as important as relative abundance of disciplines.
The red and blue dashed lines represent approximately 3- and 5-sigma confidences&mdash;signal levels near or below these thresholds should be ignored.
In other words, a document with scores only near or below the dotted lines may be considered to not be a syllabus at all&mdash;or, perhaps, be a syllabus from a discipline that cannot be represented as a superposition/overlap of existing silos.

As an example of a strong match to a single discipline, consider the tally for my upper-division Quantum Mechanics course.

![images/Sp18B3-Quantum-Syllabus-bar-score.png](images/Sp18B3-Quantum-Syllabus-bar-score.png)
![images/Sp18B3-Quantum-Syllabus-spider-score.png](images/Sp18B3-Quantum-Syllabus-spider-score.png)

Note that the machine is *not* writing a syllabus nor in any way creating a single, generic, discipline-representative syllabus.
Rather, a multidimensional search for similarity of the target syllabus to any syllabus in the training set is made; only after matches are found is the source discipline of the matched training syllabus revealed.
That is, in the decision between the following two approaches&mdash;
* *either* average all of the syllabi from a discipline
* *or* check for similarities to any of the syllabi from a discipline

&mdash;the latter has been chosen.
This is considered to be the most conservative approach because all views within a discipline may be represented.
For example, perhaps syllabi from Sociology are generally different from Anthropology syllabi.
An averaging of Sociology and Anthropology (SOAN) syllabi may wash out both Sociology and Anthropology aspects from the final product.
Instead, in the chosen search-the-whole-set approach, a minority approach to the field may still receive some matches.
The only "averaging" (i.e., suppression of minority approaches) that has been done is to set a lower limit to the number of times that a word must appear *in the entire total count from all 1600 syllabi* in order to be used for matching; that value is generally set to somewhere between 50 and 250&mdash;also, very conservative.
So, for example, the lemma "poem" may appear only in ENGL syllabi; if it appears enough (say, once in every ENGL document, on average), then it will become one of the global basis vectors (and will be quite useful for identifying ENGL syllabi!).

## Sanity checks

Here is what the classifier thinks of *Harrison Bergeron* by Vonnegut.

![images/bergeron-bar-score.png](images/bergeron-bar-score.png)

It scores quite low and so is likely not a syllabus at all; but if it is, it is some sort of literary philosophy.
(Of course, the classifier is not meant to be used to distinguish syllabi from other sorts of documents; it assumes that you are giving it a syllabus.
The low score here really means that if an instructor handed out the short story on the first day of class *as the syllabus*, a student would be justified in not being able to figure out the traditional discipline in which the course sits.
Obvious metrics *for identifying a syllabus*&mdash;such as listing office hours or an attendance policy&mdash;have been intentionally purged from this corpus.)


Similarly, the first ten pages of *50 Shades of Grey* does not score well as a syllabus, but has some hints of disciplinarity.

![images/50shades-spider-score.png](images/50shades-spider-score.png)


And, to really firm up the ROC curve, here is *The Jabberwocky*:

![images/jabberwocky-spider-score.png](images/jabberwocky-spider-score.png)

