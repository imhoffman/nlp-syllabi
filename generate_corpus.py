import textract
import os
import pickle

disciplines = [ s for s in os.listdir("web_syllabi") ]

##  code for finding bad pdf documents
#for r in disciplines:
#    for s in os.listdir( "web_syllabi/%s" % r ):
#        if "pdf" in s:
#            print( "working on: %s" % s )
#            textract.process( "web_syllabi/%s/%s" % (r,s), method='pdftotext', language='eng', encoding='ascii' ).decode( 'ascii' )

syllabi =  tuple(
        [
         ( r,
           textract.process( "web_syllabi/%s/%s" % (r,s), method='pdftotext', language='eng', encoding='ascii' ).decode( 'ascii' ) ,
           textract.process( "web_syllabi/%s/%s" % (r,s), method='pdfminer', language='eng', encoding='ascii' ).decode( 'ascii' ) ,
           textract.process( "web_syllabi/%s/%s" % (r,s), method='tesseract', language='eng', encoding='ascii' ).decode( 'ascii' )
            )
         for r in disciplines 
         for s in os.listdir( "web_syllabi/%s" % r ) if "pdf" in s
        ]
        )

output_file = open( "syllabi_texts.pckl", "wb" )
pickle.dump( syllabi, output_file )
output_file.close()


