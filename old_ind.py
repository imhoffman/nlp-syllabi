import spacy
import textract
import os
from collections import defaultdict, namedtuple
from gensim import models, similarities, corpora
#from gensim.matutils import sparse2full
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

##  chose a model --- https://spacy.io/models
nlp = spacy.load("en_core_web_lg")
#nlp = spacy.load("en_core_web_md")
#nlp = spacy.load("en_pytt_distilbertbaseuncased_lg")
#nlp = spacy.load("en_core_web_sm")
#nlp = spacy.load("en_pytt_xlnetbasecased_lg")
#nlp = spacy.load("en_pytt_bertbaseuncased_lg")

##  remove these here?  or let gensim remove them as extreme ?
##  https://stackoverflow.com/questions/41170726/add-remove-stop-words-with-spacy
more_stops = [ "course", "grade", "mark", "office", "assignment", "email", "use",
        "etc", "student", "week", "day", "date" ]
for s in more_stops:
    nlp.vocab[s].is_stop = True

##  create a struct (ie, namedtuple) for the data, recording their Discipline
##  should I keep the raw text around?  probably not...
syllabus_struct = namedtuple( 'Syllabi', [ 'discipline', 'doc' ] )
#syllabus_struct = namedtuple( 'Syllabi', [ 'discipline', 'txt', 'doc' ] )

disciplines = [ s for s in os.listdir("web_syllabi") ]

##  code for finding bad pdf documents
#for r in disciplines:
#    for s in os.listdir( "web_syllabi/%s" % r ):
#        if "pdf" in s:
#            print( "working on: %s" % s )
#            textract.process( "web_syllabi/%s/%s" % (r,s), method='pdftotext', language='eng', encoding='ascii' ).decode( 'ascii' )


##  create a list of filtered and lemmatized spacy docs from the texts
def keeper( s ):
    return ( s.is_alpha and not ( s.is_space or s.is_punct or s.is_stop or s.like_num ) )

def lemmas( doc ):
    return [ word.lemma_ for word in doc if keeper( word ) ]
    # a stop word will sneak through if the pre-lemmatized word is not on the stop list

def bases( doc ):
    return lemmas( doc )

##  select extraction method for reading PDFs
##   applied in the reading of the corpus and of the test targets
textract_method = "pdftotext"
#textract_method = "pdfminer"
#textract_method = "tesseract"

##  the single-element txt "loop" is a poor man's Python `let`
syllabi = [
        syllabus_struct( doc=lemmas( nlp( txt ) ), discipline=r )
        #syllabus_struct( doc=lemmas( nlp( txt ) ), discipline=r, txt=txt )
        for r in disciplines 
        for s in os.listdir( "web_syllabi/%s" % r ) if "pdf" in s
        for txt in [ textract.process( "web_syllabi/%s/%s" % (r,s), method=textract_method, language='eng', encoding='ascii' ).decode( 'ascii' ) ]
        ]

##  count number of syllabi per discipline for scaling later
syllabus_counts = defaultdict( int )
for s in syllabi:
    for r in disciplines:
        if s.discipline == r:
            syllabus_counts[ r ] += 1

docs = [ s.doc for s in syllabi ]

##  generate a count of the words and replace `docs` with a pruned/filtered list
freqs = defaultdict( int )
for doc in docs:
    for word in doc:
        freqs[ word ] += 1

docs = [
        [ word for word in doc
            if freqs[ word ] > int( len(syllabi)/len(disciplines) ) and freqs[ word ] < int( len(syllabi) ) ]
        for doc in docs
        ]


##
##  gensim
##

ind_dictionary = corpora.Dictionary( docs )
print( f"\n The dictionary has {len(ind_dictionary):d} words from {len(syllabi):d} syllabi.\n" )

ind_corpus = [ ind_dictionary.doc2bow(doc) for doc in docs ]
corpora.MmCorpus.serialize('/mnt/sda2/scratch/nlp/web_syllabi.mm', ind_corpus)
ind_tfidf = models.TfidfModel( ind_corpus, id2word = ind_dictionary )

##  http://dsgeek.com/2018/02/19/tfidf_vectors.html
#docs_tfidf  = ind_tfidf[ ind_corpus ]
#docs_vecs   = np.vstack( [ sparse2full( c, len(docs_dict) ) for c in docs_tfidf ] )

#tfidf_emb_vecs = np.vstack( [ nlp( docs_dict[i] ).vector for i in range(len(docs_dict)) ] )
#docs_emb = docs_vecs @ tfidf_emb_vecs



##
##  classification of syllabi using the preceding model data
##

##  criterion for a match
similarity_cutoff = 0.100

index = similarities.SparseMatrixSimilarity(
             ind_tfidf[ ind_corpus ],
             num_features = len( ind_dictionary ) )


##  function for generating scores and plots
def matches( filename, simth=similarity_cutoff ):
    ##  assemble gensim scores
    new_doc = lemmas( nlp( textract.process( filename, method=textract_method, language='eng', encoding='ascii' ).decode( 'ascii' ) ) )
    new_vec = ind_dictionary.doc2bow( new_doc )
    modeled_vec = ind_tfidf[ new_vec ]
    sims = index[ modeled_vec ]

    ##  dictionary of scores by discipline
    match_score = defaultdict( int )
    for (i,b) in list( enumerate( sims ) ):
        if b > simth:
            r = syllabi[i].discipline
            match_score[ r ] += b / syllabus_counts[ r ] * np.mean(
                                    [ n for n in syllabus_counts.values() ] )
    print( f"\n {filename:s} is similar (>{simth:.3f}) to:\n", match_score, "\n" )

    ##  bar chart
    horiz = np.arange( len( match_score ) )
    vert = match_score.values()
    plt.bar( horiz, vert, color='g' )
    plt.axhline( 1.0*simth, linestyle=':', color='r' )
    plt.axhline( 3.0*simth, linestyle=':', color='b' )
    plt.xticks( horiz, [ "(%d) %s" % (syllabus_counts[a],a) for a in match_score.keys() ], rotation='vertical' )
    plt.ylabel( "sum of similarity scores above %.3f" % simth )
    plt.title( filename )
    plt.tight_layout()
    #plt.show()
    plt.savefig( "%s%s" % (filename[:-4],"-bar-score.png"), dpi="figure", papertype="letter",
            format="png", transparent=False, bbox_inches=None,
            pad_inches=0.1, edgecolor="None", metadata=None)
    plt.close() 

    ##  spider chart
    fig = plt.figure()
    ax = fig.add_subplot( 111, polar=True )
    angles = np.linspace( 0.0, 2.0*np.pi, len( match_score ), endpoint = False )
    labels = [ s for s in match_score.keys() ]
    radii  = [ x for x in match_score.values() ]
    angles = np.concatenate( ( angles, angles[0] ), axis = None )
    radii  = np.concatenate( (  radii,  radii[0] ), axis = None )
    ax.plot( angles, radii, 'o-', linewidth = 2 )
    ax.fill( angles, radii, alpha = 0.25 )
    ax.plot( np.linspace( 0.0, 2.0*np.pi, 1024 ),
            [ 1.0*simth for i in range(1024) ], linestyle=':', color='r' )
    ax.plot( np.linspace( 0.0, 2.0*np.pi, 1024 ),
            [ 3.0*simth for i in range(1024) ], linestyle=':', color='b' )
    ax.set_thetagrids( angles * 180.0/np.pi, labels )
    ax.set_title( filename )
    plt.tight_layout()
    #plt.show()
    plt.savefig( "%s%s" % (filename[:-4],"-spider-score.png"), dpi="figure", papertype="letter",
                format="png", transparent=False, bbox_inches=None, pad_inches=0.1,
                edgecolor="None", metadata=None)
    plt.clf()
    plt.close()


##  apply the matching function to some test syllabi
matches( "Sp18B3-Quantum-Syllabus.pdf" )
matches( "Fa19B2-Programming-Syllabus.pdf" )
matches( "Fa19B4-Advent-Syllabus.pdf" )

[ matches( "test/%s" % s ) for s in os.listdir("test") if "pdf" in s ]


